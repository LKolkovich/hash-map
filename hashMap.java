import java.util.ArrayList;

public class hashMap<K,V>{
    private ArrayList<K> keys = new ArrayList<>();
    private ArrayList<V> values = new ArrayList<>();
    private int size = 0;

    public hashMap(){}

    public void put(K key, V value){
        boolean flag = true;
        for(int i = 0; i < size; i++){
            if(keys.get(i) == key){
                values.set(i, value);
                flag = false;
            }
        }
        if(flag){
            keys.add(key);
            values.add(value);
            size++;
        }
    }

    public V get(K key){
        for(int i = 0; i < size; i++){
            if(keys.get(i) == key){
                return values.get(i);
            }
        }
        return null;
    }

    public ArrayList KeySet(){
        return keys;
    }

    public ArrayList values(){
        return values;
    }

    public boolean containsKey(K key){
        for(int i = 0; i < size; i++){
            if(keys.get(i) == key)
            {
                return true;
            }
        }
        return false;
    }

    public boolean containsValue(V value){
        for(int i = 0; i < size; i++){
            if(values.get(i) == value)
            {
                return true;
            }
        }
        return false;
    }

    public boolean isEmpty(){
        return size != 0;
    }

    public boolean remove(K key){
        for(int i = 0; i < size; i++){
            if(keys.get(i) == key){
                keys.remove(i);
                values.remove(i);
                size--;
                return true;
            }
        }
        return false;
    }

    public int size(){
        return size;
    }

    public void clear(){
        keys = new ArrayList<>();
        values = new ArrayList<>();
    }
}